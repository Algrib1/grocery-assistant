"""
Grocery assistant.
Note, it may be only 0-30 apples.
If other quantity is required, the answer is "Столько нет"
"""
import termcolor


def main():
    """Add "яблоко" word."""
    n = int(input("Сколько яблок Вам нужно?\n"))

    range_1 = range(0, 31)
    ending = "яблок"

    if (n % 10 == 1):
        ending = "яблоко"
    elif (n % 10 in (2, 3, 4)):
        ending = "яблока"

    if (n % 100 in (11, 12, 13, 14)):
        ending = "яблок"

    if (n in range_1):
        print("Пожалуйста,", n, ending)
    else:
        print("Столько нет")


if __name__ == "__main__":
    print(termcolor.colored("Grocery assistant", 'red'))  # color this caption
    main()
